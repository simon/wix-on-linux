#include <stdint.h>
#include <uchar.h>

uint32_t MsiGetFileVersionW(const char16_t *filename,
                            char16_t *version, uint32_t *version_size,
                            char16_t *language, uint32_t *language_size);
