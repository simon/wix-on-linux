#include <err.h>

#include "memory.h"

void *smalloc(size_t size)
{
    void *toret = malloc(size);
    if (!toret)
        errx(1, "out of memory");
    return toret;
}

void *srealloc(void *ptr, size_t size)
{
    void *toret = realloc(ptr, size);
    if (!toret)
        errx(1, "out of memory");
    return toret;
}
