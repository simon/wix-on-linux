#!/usr/bin/env python3

import sys
import os

def addtopath(val, varname):
    newvar = val
    if varname in os.environ:
        if val in os.environ[varname].split(":"):
            return # already there
        newvar += ":" + os.environ[varname]
    os.environ[varname] = newvar

scriptname = os.path.basename(sys.argv[0])
wixdir = os.path.dirname(os.path.abspath(__file__))

addtopath(os.path.join(wixdir, "libpreload.so"), "LD_PRELOAD")

os.environ["WIX"] = wixdir

dotnet_exe = os.path.join(wixdir, scriptname + ".exe")
os.execvp("/usr/bin/mono", ["mono", dotnet_exe] + sys.argv[1:])
