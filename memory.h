#include <stdlib.h>

void *smalloc(size_t size);
void *srealloc(void *ptr, size_t size);

#define snew(type) ((type *)smalloc(sizeof(type)))
#define snewn(n,type) ((type *)smalloc((n)*sizeof(type)))
#define sresize(ptr,n,type) ((type *)srealloc(ptr,(n)*sizeof(type)))
#define sfree(ptr) free(ptr)
