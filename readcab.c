#include <stdint.h>
#include <stdbool.h>
#include <uchar.h>
#include <err.h>

#include "memory.h"
#include "subproc.h"
#include "uchars.h"

uint32_t ExtractCabBegin(void)
{
    return 0;
}

uint32_t ExtractCab(const char16_t *wzCabinet, const char16_t *wzExtractDir)
{
    char *cab = ascii(wzCabinet, true), *dir = ascii(wzExtractDir, true);
    warnx("ExtractCab(\"%s\", \"%s\"", cab, dir);
    system_argv("cabextract", "-d", dir, cab, cNULL);
    sfree(cab);
    sfree(dir);
    return 0;
}

void ExtractCabFinish(void)
{
}

uint32_t EnumerateCabBegin(void)
{
    return 0;
}

uint32_t EnumerateCab(const char16_t *wzCabinet, void *pfnNotify)
{
    return -1;                        /* we don't support this call */
}

void EnumerateCabFinish(void)
{
}
