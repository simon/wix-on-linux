#include <stddef.h>
#include <stdarg.h>
#include <string.h>
#include <err.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "subproc.h"

void system_argv_array(char **args)
{
    pid_t pid = fork();
    if (pid < 0)
        err(1, "fork");

    if (pid == 0) {
        execvp(args[0], args);
        warn("execvp");
        _exit(127);
    }

    int status;
    if (waitpid(pid, &status, 0) != pid)
        err(1, "waitpid");
    if (!(WIFEXITED(status) && WEXITSTATUS(status) == 0))
        errx(1, "subcommand failed");
}

void system_argv(const char *cmd, ...)
{
    int nargs, nchars;
    const char *word;
    va_list ap;

    va_start(ap, cmd);
    nargs = 1;                         /* terminating NULL */
    nchars = 0;
    for (word = cmd; word; word = va_arg(ap, const char *)) {
        nargs++;
        nchars += 1 + strlen(word);
    }
    va_end(ap);

    char *args[nargs], chars[nchars];
    char **argp = args, *charp = chars;
    va_start(ap, cmd);
    for (word = cmd; word; word = va_arg(ap, const char *)) {
        *argp++ = charp;
        strcpy(charp, word);
        charp += 1 + strlen(word);
    }
    va_end(ap);
    *argp++ = NULL;

    system_argv_array(args);
}

