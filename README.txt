WiX 3 cross-building support for Linux
======================================

This directory contains a suite of Linux shared libraries and
supporting scripts to allow parts of the WiX 3 toolset to run on
Linux, generating MSI Windows installer files without depending on
Windows itself, any official Windows DLLs, or even Wine.

Requirements
------------

This system has been tested only on Ubuntu, but it has been running
reliably for me on all stable versions of Ubuntu from 14.04 to 22.04.

To run, you will need some Ubuntu packages installed:

 - 'mono-runtime' (to run the main CLR executables from WiX itself)
 - 'cabextract' and 'msitools' (the libraries in this suite will
   expect to be able to invoke them via fork/exec)
 - Python 3 to run the wrapper.py and makecab.py scripts that are part
   of this source base.

To compile it, you will also need gcc and autotools.

(There may be other package dependencies I haven't spotted. I've only
tested this on my own machine and haven't done a full analysis of what
might have already been installed here that I didn't realise I was
depending on. If you find problems along these lines, please help make
this section more complete.)

Installation
------------

First, get the official WiX distribution. I downloaded the latest WiX
3 zip file at the time, which was a zip file called wix310-binaries.zip.

Unpack that zip file into a directory of your choice, which I will
refer to below as $WIX_INSTALL_DIR.

Now build and install the stuff in _this_ directory into that same
directory, by running

 - ./autogen.sh (assuming you checked this source tree out of git
   rather than installing a tarball of it, so that 'configure' won't
   already exist)

 - ./configure --bindir=$WIX_INSTALL_DIR

 - make

 - make install

This will install some Python scripts and some .so files into the same
location $WIX_INSTALL_DIR where you unpacked the WiX zip file. In
particular, you should find two files 'candle' and 'light' - actually
symlinks to wrapper.py - dropped into that directory, alongside the
candle.exe and light.exe that came from the WiX distribution.

Running it
----------

You should now be able to run $WIX_INSTALL_DIR/candle and
$WIX_INSTALL_DIR/light more or less as if they were the ordinary WiX
'candle' and 'light' programs.

Ordinary WiX installer source should basically work, except that as a
consequence of running the WiX CLR code on a different OS, this system
will not tolerate backslashes used as directory separators in
pathnames in the .wxs file. You'll need to replace any of those with
forward slashes first (which in my experience does not break
compatibility with WiX run in the usual way).

Those scripts will run candle.exe and light.exe under Mono, and
arrange to load my .so files in place of the native-code Windows DLLs
that those programs need; the .so files will in turn call out to
cabextract, msibuild and my makecab.py script as necessary.

Limitations
-----------

This system was set up for the stable WiX 3. It's never been tested
with WiX 4.

It is extremely possible that some more advanced features of WiX will
not work in this system. I will be interested to hear about them, if
so, but I may not have time and energy to fix them; patches would be
preferable to plain bug reports.

Licence
-------

The contents of this directory are copyright 2017 Simon Tatham, except
for md5.c, which contains code from PuTTY written in 1999 (also
copyright Simon Tatham). It is redistributable under the MIT licence.
See the file LICENCE for the full licence text.
