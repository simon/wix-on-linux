#include <stddef.h>

#ifndef cNULL
#define cNULL ((const char *)NULL)
#endif

char *dupstr(const char *str);
char *dupcat(const char *str, ...);
