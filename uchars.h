#include <uchar.h>
#include <stdint.h>
#include <stdbool.h>

char *ascii(const char16_t *wstr, bool translate_slashes);
void c16cpy(char16_t *out, uint32_t *outsize, const char *s);
