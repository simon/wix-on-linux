#include "memory.h"
#include "uchars.h"

char *ascii(const char16_t *wstr, bool translate_slashes)
{
    size_t len = 0;
    for (const char16_t *wp = wstr; *wp; wp++)
        len++;
    char *ret = snewn(len + 1, char);
    char *p = ret;
    for (const char16_t *wp = wstr; *wp; wp++)
        *p++ = (*wp == '\\' && translate_slashes ? '/' :
                *wp < 0x80 ? *wp :
                '?');
    *p = '\0';
    return ret;
}

void c16cpy(char16_t *out, uint32_t *outsize, const char *s)
{
    uint32_t retlen = 0;
    while (retlen < *outsize) {
        char16_t c = (out[retlen] = (unsigned char)*s++);
        if (!c)
            break;
        retlen++;
    }
    *outsize = retlen;
}
