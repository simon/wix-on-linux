#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>

#include "misc.h"
#include "uchars.h"
#include "api.h"

int main(int argc, char **argv)
{
    if (argc > 2 && !strcmp(argv[1], "version")) {
        const char *filename8 = argv[2];
        char16_t filename16[4096];
        uint32_t filename16size = lenof(filename16);
        c16cpy(filename16, &filename16size, filename8);
        char16_t version[4096], language[4096];
        uint32_t versionsize = lenof(version), languagesize = lenof(language);
        uint32_t retd =
            MsiGetFileVersionW(filename16, version, &versionsize,
                               language, &languagesize);
        printf("MsiGetFileVersionW(\"%s\") = %"PRId32"\n", filename8, retd);
        if (retd == 0) {
            printf("  version = \"%s\"\n  language = \"%s\"\n",
                   ascii(version, false), ascii(language, false));
        }
    } else {
        fprintf(stderr, "usage: ./test version <file>\n");
        return 1;
    }
    return 0;
}
