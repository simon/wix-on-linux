#include <stdarg.h>
#include <string.h>

#include "memory.h"
#include "dupstr.h"

char *dupcat(const char *str, ...)
{
    va_list ap;
    const char *p;
    char *out, *outp;
    size_t len;

    len = 1;
    va_start(ap, str);
    for (p = str; p; p = va_arg(ap, const char *))
        len += strlen(p);
    va_end(ap);

    out = snewn(len, char);
    outp = out;
    va_start(ap, str);
    for (p = str; p; p = va_arg(ap, const char *)) {
        strcpy(outp, p);
        outp += strlen(p);
    }
    va_end(ap);

    return out;
}

char *dupstr(const char *str)
{
    return dupcat(str, (const char *)NULL);
}
